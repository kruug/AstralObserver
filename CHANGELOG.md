# History/Changelog

- 2018-04-05: Finished class to read position from the telescope.  This class updates the global variables to ensure all classes and forms have the same data.  Added the Horizon to Equatorial calculation class.
- 2017-12-04: Added Gregorian to Julian calculation process.  Added Vernier menu and should be functional.  Added a Notes window.  Not sure where it's actually called, but good to have as it also serves as the documentation.  Will have to re-evaluate as it points out that it doesn't use the proper daylight savings schedule.
- 2017-12-01: Got the program constants set up.  Not sure how much of this is needed, but good to have added to be safe.
- 2017-10-27: Added the Move/Track menu.  No functions were fleshed out, but getting the GUI done.
- 2017-10-26: Worked on I/O Test class.  Added general messages class.  Added special controls menu.  Doesn't look like anything that the buttons describe was actually included in the original code.  Sadly, Mr. Elbert is no longer alive to consult with.
- 2017-10-25: Created documentation for contribution best practices.  Added Code of Conduct.  Added License file. Added Changelog file. Added Readme.  Started on Splash Screen, Main Menu, and I/O Test class.