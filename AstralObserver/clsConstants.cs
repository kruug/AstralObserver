﻿/*=============================================================================
 |       Module:  clsConstants.cs
 |
 |       Author:  kruug
 |     Language:  C#
 |   To Compile:  Nothing special needed.
 |
 |         Date:  2017-12-01
 |      Updated:  2017-12-01
 |
 +-----------------------------------------------------------------------------
 |
 |  Description:  Set constants in program settings.  This could all be set
 |                  manually in program settings, but this allows the use of
 |                  math and other dynamic capabilities.
 |
 |        Input:  None
 |
 |       Output:  Nothing.  Only sets program settings.
 |
 |    Algorithm:  Nothing special.
 |
 |   Known Bugs:  None.
 |
 |        Notes:  
 |
 *===========================================================================*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AstralObserver
{
    class clsConstants
    {
        public void initConstants()
        {
            Properties.Settings.Default.lat_deg = 44.81588891;  // Beaver Creek Latitude, fixed 1997-02-09
            Properties.Settings.Default.long_deg = 91.27183333; // Beaver Creek Longitude, fixed 1997-02-09
            //Properties.Settings.Default.pi = 3.14159265;
            Properties.Settings.Default.pi = 3.14159265358979323846;
            Properties.Settings.Default.min_f = 16.66666666E-3;
            Properties.Settings.Default.sec_f = 277.77777777E-6;

            Properties.Settings.Default.deg_to_rad = Properties.Settings.Default.pi / 180.0;
            Properties.Settings.Default.rad_to_deg = 180.0 / Properties.Settings.Default.pi;
            Properties.Settings.Default.hor_to_deg = 360.0 / 23.93447219;
            Properties.Settings.Default.sec_rad = Properties.Settings.Default.sec_f * Properties.Settings.Default.hor_to_deg * Properties.Settings.Default.deg_to_rad;
            Properties.Settings.Default.sids_rad = Properties.Settings.Default.sec_rad * 1.0027379093;

            Properties.Settings.Default.gen_flag = 0;
            Properties.Settings.Default.vern_flag = 0;
            Properties.Settings.Default.zaz_flag = 0;  // Flag to toggle 0, -1, for ZeroAZM problem
            Properties.Settings.Default.pc_flag = 0;
            Properties.Settings.Default.alt_cd = 0.0; // Zero corrections at init
            Properties.Settings.Default.azm_cd = 0.0;

            Properties.Settings.Default.lat_rad = Properties.Settings.Default.lat_deg * Properties.Settings.Default.deg_to_rad;
            Properties.Settings.Default.long_rad = Properties.Settings.Default.long_deg * Properties.Settings.Default.deg_to_rad;
            Properties.Settings.Default.cos_lat = Math.Cos(Properties.Settings.Default.lat_deg);
            Properties.Settings.Default.sin_lat = Math.Sin(Properties.Settings.Default.lat_deg);

            /* Note: This is TWO SEC track version */

            Properties.Settings.Default.rs_alt_deg = 1400.0 / 720.0; // rev/sec factor per altitude degree
            Properties.Settings.Default.rs_azm_deg = 6987.0 / 720.0; // rev/sec factor per azimuth degree
            Properties.Settings.Default.ppd_alt = -(25000.0 * 1400.0) / 360.0; // pulses per +degree altitude
            Properties.Settings.Default.ppd_azm = -(25000.0 * 6987.0) / 360.0; // pulses per +degree azimuth

            Properties.Settings.Default.hor_alt_deg = 0.0; // Home position, altitude, degrees
            Properties.Settings.Default.hor_azm_deg = 0.0; // Home position, azimuth, degrees

            Properties.Settings.Default.tel_alt_deg = Properties.Settings.Default.hor_alt_deg; // Telescope current altitude degrees
            Properties.Settings.Default.tel_azm_deg = Properties.Settings.Default.hor_azm_deg; // Telescope current azimuth degrees

            Properties.Settings.Default.alt_cd = Properties.Settings.Default.hor_alt_deg; // Display altitude
            Properties.Settings.Default.azm_cd = Properties.Settings.Default.hor_azm_deg; // Display azimuth

            Properties.Settings.Default.p_ha_r = Properties.Settings.Default.hor_azm_deg * Properties.Settings.Default.deg_to_rad;
            Properties.Settings.Default.p_dec_r = (Properties.Settings.Default.lat_deg - 90.0) * Properties.Settings.Default.deg_to_rad;

            Properties.Settings.Default.Save();
        }
    }
}
