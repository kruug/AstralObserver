﻿/*=============================================================================
 |       Module:  winVernier.xaml.cs
 |
 |       Author:  kruug
 |     Language:  C#
 |   To Compile:  Nothing special needed.
 |
 |         Date:  2017-12-04
 |      Updated:  2017-12-04
 |
 +-----------------------------------------------------------------------------
 |
 |  Description:  Modifies the vernier rates
 |
 |        Input:  None
 |
 |       Output:  Nothing.  Only sets program settings.
 |
 |    Algorithm:  Nothing special.
 |
 |   Known Bugs:  None.
 |
 |        Notes:  
 |
 *===========================================================================*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AstralObserver
{
    /// <summary>
    /// Interaction logic for winVernier.xaml
    /// </summary>
    public partial class winVernier : Window
    {
        public winVernier()
        {
            InitializeComponent();
        }

        private void btnOnePix_Click(object sender, RoutedEventArgs e)
        {
            Properties.Settings.Default.vern_flag = 0;
            this.Close();
        }

        private void btnTenPix_Click(object sender, RoutedEventArgs e)
        {
            Properties.Settings.Default.vern_flag = 1;
            this.Close();
        }

        private void btn40Pix_Click(object sender, RoutedEventArgs e)
        {
            Properties.Settings.Default.vern_flag = 2;
            this.Close();
        }

        private void btn100Pix_Click(object sender, RoutedEventArgs e)
        {
            Properties.Settings.Default.vern_flag = 3;
            this.Close();
        }
    }
}
