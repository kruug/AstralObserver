﻿/*=============================================================================
 |       Module:  winNotes.xaml.cs
 |
 |       Author:  kruug
 |     Language:  C#
 |   To Compile:  Nothing special needed.
 |
 |         Date:  2017-12-04
 |      Updated:  2017-12-04
 |
 +-----------------------------------------------------------------------------
 |
 |  Description:  Notes on how to use the program.
 |
 |        Input:  No input.
 |
 |       Output:  GUI of notes.
 |
 |    Algorithm:  GUI is built with XAML in Visual Studio
 |
 |   Known Bugs:  None so far, except only being able to run on Windows.
 |
 |        Notes:  None.
 |
 *===========================================================================*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AstralObserver
{
    /// <summary>
    /// Interaction logic for winNotes.xaml
    /// </summary>
    public partial class winNotes : Window
    {
        string textOne;
        string textTwo;
        string textThree;
        string textFour;
        string textFive;
        string textSix;
        string textSeven;
        string textEight;
        string textNine;
        string textTen;
        string TextEleven;

        public winNotes()
        {
            InitializeComponent();
            textOne = "Upon system start-up, make sure that the TELESCOPE is positioned such that the HOME MARKERS provided on the azimuth & elevation axes & CompuMotor shafts are precisely aligned.";
            textTwo = "Turn the telescope power ON.  Then, using the STARTRAC opening menu,";
            textThree = "1 - Set the system DATE and TIME";
            textFour = "2 - RESET the 2100 Indexer";
            textFive = "3 - INITIALIZE the system to the HOME position";
            textSix = "[NOTE: Local TIME should be set to the accuracy provided by NBS Station WWV.  For this program, CDT is in effect from April 1 to Oct 31, else use CST.]";
            textSeven = "The system should now be aligned accurately enough to place an object within the CCD Viewport by entering its Right Ascension and Declination from the MOVE/TRACK menu.";
            textEight = "Exact alignment of the telescope, center pixel of the CCD, and the object being viewed can easily be made (from the tracking mode) by using the Vernier controls.  Re-initialization of the system from this point will precisely correct the initial system alignment parameters to the currently viewed celestial sphere.";
            textNine = "CONVENTIONS";
            textTen = "AZIMUTH: South = 0 deg, West = 90 deg, North = 180 deg, East = 270 deg";
            TextEleven = "ALTITUDE: Horizon = 0 deg, Zenity = 90 deg";

            txtNotes.Text = textOne + '\n' + '\n' + textTwo + '\n' + textThree + '\n' + textFour + '\n' + textFive + '\n' + '\n' + textSix + '\n' + '\n' + textSeven + '\n' + '\n' + textEight + '\n' + '\n' + textNine + '\n' + '\n' + textTen + '\n' + '\n' + TextEleven;
        }
    }
}
