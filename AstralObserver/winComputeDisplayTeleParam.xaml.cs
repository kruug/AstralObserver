﻿/*=============================================================================
 |       Module:  winComputeDisplayTeleParam.cs
 |
 |       Author:  kruug
 |     Language:  C#
 |   To Compile:  Nothing special needed.
 |
 |         Date:  2018-07-17
 |      Updated:  2018-07-17
 |
 +-----------------------------------------------------------------------------
 |
 |  Description:  This window computes and displays telescope parameters.
 |                  0 sets up initial window and display.  Bit 1 "on" is normal
 |                  entry, bit 2 "on" updates teh positions display, bit 3 "on"
 |                  puts `---` in positions display.  Gets current time
 |                  (cestime), computes current sidereal time, and displays
 |                  current date, time, and sidereal time.  Saves `cestime` in
 |                  `lestime`, for loop check at next pass.
 |
 |        Input:  Telescope mode
 |
 |       Output:  Nothing except a GUI menu.
 |
 |    Algorithm:  GUI is built with XAML in Visual Studio
 |
 |   Known Bugs:  None so far, except only being able to run on Windows.
 |
 |        Notes:  None.
 |
 *===========================================================================*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AstralObserver
{
    /// <summary>
    /// Interaction logic for winComputeDisplayTeleParam.xaml
    /// </summary>
    public partial class winComputeDisplayTeleParam : Window
    {
        double azmd = Properties.Settings.Default.azm_deg;
        double altd = Properties.Settings.Default.alt_deg;
        double phar = Properties.Settings.Default.p_ha_r;
        double pdecr = Properties.Settings.Default.p_dec_r;
        double csidtm = Properties.Settings.Default.c_sidereal_time;
        double sidsec = Properties.Settings.Default.sidereal_second;
        double rtod = Properties.Settings.Default.rad_to_deg;
        DateTime lestime;
        int sidhr = Properties.Settings.Default.sidereal_hour;
        int sidmin = Properties.Settings.Default.sidereal_minute;
        int tzone = Properties.Settings.Default.timezone;

        char[] ltimestr = new char[12]; /* Defined here as global for local time logger use */

        public winComputeDisplayTeleParam(int mode)
        {
            InitializeComponent();

            DateTime cestime = DateTime.Now;
            //struct tm *cdtmptr;
            char[] stimstr = new char[12];
            char[] cdatstr = new char[16];

            int i;
            int sidisec;

            double fsidsec;
            double dtime;
            double phafd;
            double pdecfd;

            if (mode == 0) /* Initial entry, set up window */
            {
                /* Check if an instance of this window is already open and close if there is */
                if (IsWindowOpen<Window>("winComputeDisplayTeleParam"))
                {
                    // MyWindowName is open
                    Console.Write(Application.Current.Windows.OfType<Window>().Any());
                }

                
            }
        }

        public static bool IsWindowOpen<T>(string name = "") where T : Window
        {
            return string.IsNullOrEmpty(name)
               ? Application.Current.Windows.OfType<T>().Any()
               : Application.Current.Windows.OfType<T>().Any(w => w.Name.Equals(name));
        }
    }
}
