﻿/*=============================================================================
 |       Module:  MainWindow.cs
 |
 |       Author:  kruug
 |     Language:  C#
 |   To Compile:  Nothing special needed.
 |
 |         Date:  2017-10-25
 |      Updated:  2017-10-27
 |
 +-----------------------------------------------------------------------------
 |
 |  Description:  This is the main menu.  This will provide access to all other
 |                  functions of the program.
 |
 |        Input:  No input.
 |
 |       Output:  Nothing except a GUI menu.
 |
 |    Algorithm:  GUI is built with XAML in Visual Studio
 |
 |   Known Bugs:  None so far, except only being able to run on Windows.
 |
 |        Notes:  
 |
 *===========================================================================*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AstralObserver
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        clsMessages messages = new clsMessages();
        clsConstants constants = new clsConstants();

        public MainWindow()
        {
            System.Threading.Thread.Sleep(5000);
            InitializeComponent();

            constants.initConstants();
            /*
             * TODO: Implement tmc_tsio() to test for I/O connection, Telescope Power On & Response
             * If no I/O connection, throw error
             * If I/O connection,
             *   Implement tmc_inms() to initialize motor state
             *   s_gconst() to define/compute gen global constants
             *   t_sidtim() to initialize sidereal, and via call, std time & parm disp
             */
        }

        private void btnResetIndexer_Click(object sender, RoutedEventArgs e)
        {
            messages.resetCompuMotor();
        }

        private void btnZeroCoordCorrections_Click(object sender, RoutedEventArgs e)
        {
            messages.zeroCoordinateCorrections();
        }

        private void btnManualCommand_Click(object sender, RoutedEventArgs e)
        {
            //TODO: Not defined in original program
        }

        private void btnSpecialControlsMenu_Click(object sender, RoutedEventArgs e)
        {
            //TODO: Implement mw_spclm()
            winSpecialControlsMenu spclm = new winSpecialControlsMenu();
            spclm.ShowDialog();
        }

        private void btnMoveTrk_Click(object sender, RoutedEventArgs e)
        {
            //TODO: Implement mw_movtm()
            winMoveTrackMenu movtm = new winMoveTrackMenu();
            movtm.ShowDialog();
        }

        private void btnKeyOperators_Click(object sender, RoutedEventArgs e)
        {
            //TODO: Not defined in original program
        }

        private void btnQuit_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }
    }
}
