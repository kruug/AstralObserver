﻿/*=============================================================================
 |       Module:  clsHorizonToEquatorial.cs
 |
 |       Author:  kruug
 |     Language:  C#
 |   To Compile:  Nothing special needed.
 |
 |         Date:  2018-04-05
 |      Updated:  2018-04-05
 |
 +-----------------------------------------------------------------------------
 |
 |  Description:  Calculates horizon readings to equatorial commands.
 |
 |        Input:  Altitude and azimuth readings
 |
 |       Output:  Horizon and declination
 |
 |    Algorithm:  
 |
 |   Known Bugs:  None.
 |
 |        Notes:  Used by routines m_mazalt() and m_mjoyst() to derive horizon
 |                  & declination from altitude and azimuth.
 |
 *===========================================================================*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AstralObserver
{
    class clsHorizonToEquatorial
    {
        double k1 = Properties.Settings.Default.k1;
        double k2 = Properties.Settings.Default.k2;
        double k3 = Properties.Settings.Default.k3;
        double k4 = Properties.Settings.Default.k4;
        double k5 = Properties.Settings.Default.k5;
        double pi = Properties.Settings.Default.pi;
        double altd = Properties.Settings.Default.alt_deg;
        double azmd = Properties.Settings.Default.azm_deg;
        double cosd = Properties.Settings.Default.cos_deg;
        double dtor = Properties.Settings.Default.deg_to_rad;
        double phar = Properties.Settings.Default.p_ha_r;
        double pdecr = Properties.Settings.Default.p_dec_r;
        double coslat = Properties.Settings.Default.cos_lat;
        double sinlat = Properties.Settings.Default.sin_lat;
        
        void horizToEqui()
        {
            double caltr, cazmr, cosaltr, sinaltr, cosazmr, cdecr, cmhar, sind;
            double coranr, paltcr, pazmcr, kqtst;

            caltr = altd * dtor;
            cazmr = azmd * dtor;

            // Remove platform fixes
            coranr = cazmr - 1.0472; // Correct Angle
            paltcr = -0.0020944 * Math.Sin(coranr); // Altitude correction
            pazmcr = -0.00200 * Math.Cos(coranr) / Math.Cos(caltr); // Azimuth correction
            caltr -= paltcr; // Altitude correction removed
            cazmr -= pazmcr; // Azimuth correction removed

            cosaltr = Math.Cos(caltr);
            sinaltr = Math.Sin(caltr);
            cosazmr = Math.Cos(cazmr);

            cdecr = Math.Asin((-coslat * cosaltr * cosazmr) + (sinlat * sinaltr));

            if (cdecr > 1.57079 && cdecr < 1.57081) //Near north pole
            {
                cmhar = 0.0;
                cosd = 0.0000001;
            } else
            {
                cmhar = Math.Asin(cosaltr * Math.Sin(cazmr) / Math.Cos(cdecr));
                cosd = Math.Cos(cdecr); // To global
            }

            sind = Math.Sin(cdecr);
            k1 = coslat * cosd;  // EQ paramters to global
            k2 = sinlat * sind;
            k3 = coslat * sind;
            k4 = sinlat * cosd;
            //k5 = coslat * cosd;

            kqtst = (sinlat * cosaltr * cosazmr) + (coslat * sinaltr);
            if (kqtst < 0.00)
            {
                cmhar = pi - cmhar; // Correct quadrant
            }

            phar = cmhar;
            pdecr = cdecr;
        }
    }
}
