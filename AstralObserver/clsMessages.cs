﻿/*=============================================================================
 |       Module:  clsMessages.cs
 |
 |       Author:  kruug
 |     Language:  C#
 |   To Compile:  Nothing special needed.
 |
 |         Date:  2017-10-26
 |      Updated:  2017-10-26
 |
 +-----------------------------------------------------------------------------
 |
 |  Description:  General message windows for the program.
 |
 |        Input:  No input.
 |
 |       Output:  Nothing except GUI messages.
 |
 |    Algorithm:  Message box objects built into .Net
 |
 |   Known Bugs:  None so far, except only being able to run on Windows.
 |
 |        Notes:  None.
 |
 *===========================================================================*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace AstralObserver
{
    class clsMessages
    {
        public void forbiddenDeclination() //wn_gmesg(0)
        {
            MessageBox.Show("Object in Forbidden Declination Region", "Forbidden Declination", MessageBoxButton.OK, MessageBoxImage.Error);
        }

        public void belowHorizon() //wn_gmesg(1)
        {
            MessageBox.Show("Selected Object is Below the Horizon", "Below Horizon", MessageBoxButton.OK, MessageBoxImage.Error);
        }

        public void zeroCoordinateCorrections() //wn_gmesg(2)
        {
            //TODO: Zero AZM & ALT coordinate corrections
            MessageBox.Show("The Coordinate Correction Values Have Been Zeroed", "Zero Coordinate Correction Values", MessageBoxButton.OK, MessageBoxImage.Information);
        }

        public void excessiveRates() //wn_gmesg(3)
        {
            MessageBox.Show("Excessive Tracking Rates - Telescope Stopped", "Excessive Tracking", MessageBoxButton.OK, MessageBoxImage.Exclamation);
        }

        public void beginTracking() //wn_gmesg(4)
        {
            MessageBoxResult track = MessageBox.Show("Begin Tracking?", "Begin Tracking?", MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (track == MessageBoxResult.Yes)
            {
                //TODO: Start Tracking
            } else
            {
                //TODO: Launch Main Menu
            }
        }

        public void resetCompuMotor() //wn_gmesg(5)
        {
            MessageBox.Show("The CompuMotor 2100 Indexer has Been Reset", "Reset CompuMotor", MessageBoxButton.OK, MessageBoxImage.Information);
            //TODO: Reset CompuMotor Indexer
        }

        public void enableManual() //wn_gmesg(6)
        {
            MessageBox.Show("Turn Joystick Power & Enable Switche ON at Telescope", "Enable Manual Mode", MessageBoxButton.OK, MessageBoxImage.Information);
        }

        public void forbiddenAltitude() //wn_gmesg(7)
        {
            MessageBox.Show("Forbidden Altitude.  Must be < 89.9997 Degrees", "Forbidden Altitude", MessageBoxButton.OK, MessageBoxImage.Error);
        }

        public void coordinateCorrections() //wn_gmesg(8)
        {
            MessageBox.Show("Corrections to Coordinates are Now in the System", "Coordinates Corrected", MessageBoxButton.OK, MessageBoxImage.Information);
        }
    }
}
