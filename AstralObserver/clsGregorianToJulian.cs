﻿/*=============================================================================
 |       Module:  clsGregorianToJulian.cs
 |
 |       Author:  kruug
 |     Language:  C#
 |   To Compile:  Nothing special needed.
 |
 |         Date:  2017-12-01
 |      Updated:  2017-12-04
 |
 +-----------------------------------------------------------------------------
 |
 |  Description:  Converts current system date from Gregorian format to
 |                  Julian format
 |
 |        Input:  Character string MM/DD/YY or MM/DD/YYYY.  YY implies 19YY;
 |                  YYYY is entire year, 2001, 1888, or 0011.  Variable indate
 |                  should be 11 characters long to handle all cases.
 |
 |       Output:  Returns long (Julian date, 1 = 1st day AD), -1 if error
 |
 |    Algorithm:  TODO!
 |
 |   Known Bugs:  None.
 |
 |        Notes:  Not sure if this is still needed on newer operating systems.
 |                  Included to ensure functionality and compatibility.
 |
 *===========================================================================*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AstralObserver
{
    class clsGregorianToJulian
    {
        public long gregorianToJulian(string indate)
        {
            int[] acmonthd = { 0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334, 365 };

            long cen_days = 36524;
            long year_days = 365;
            long jul_days;

            int i;

            int leapd = 0;
            int leapf;
            int in_year;
            int in_day;
            int in_month;
            int century_num;
            int cen_years;
            int cur_years;
            int ex_day;

            if (indate.Length == 8) // YY Format
            {
                string[] numbers = indate.Split('/');
                in_month = Convert.ToInt32(numbers[0]);
                in_day = Convert.ToInt32(numbers[1]);
                in_year = Convert.ToInt32(numbers[2]);

                if (in_year < 0 || in_year > 99)
                {
                    return -1; // Not a valid year
                }

                if (in_year == 0) // YY = 00, interpreted as 1900
                {
                    century_num = 18;
                    cen_years = 99;
                    cur_years = 100;
                } else
                {
                    century_num = 19;
                    cen_years = in_year - 1;
                    cur_years = in_year;
                }
            } else if (indate.Length == 10) // YYYY Format
            {
                string[] numbers = indate.Split('/');
                in_month = Convert.ToInt32(numbers[0]);
                in_day = Convert.ToInt32(numbers[1]);
                in_year = Convert.ToInt32(numbers[2]);

                if (in_year < 0)
                {
                    return -1; // Not a valid year
                }

                century_num = (in_year - 1) / 100;
                cen_years = in_year - 1 - century_num * 100;
                cur_years = in_year - century_num * 100;
            } else
            {
                return -1; // Wrong input format
            }

            if (in_month < 1 || in_month > 12)
            {
                return -1; // Not a valid month
            }

            if (in_day < 1 || in_day > 31)
            {
                return -1; // Not a valid day
            }

            if (cur_years % 4 == 0 && (cur_years != 0 && cur_years != 100 || (century_num + 1) % 4 == 0))
            {
                leapf = 1;
            } else
            {
                leapf = 0;
            }

            if (leapf == 1 && in_month == 2)
            {
                ex_day = 1;
            } else
            {
                ex_day = 0;
            }

            if (in_day > (acmonthd[in_month] - acmonthd[in_month - 1] + ex_day))
            {
                return -1; // Error
            }

            // Up to but not including current century
            jul_days = century_num * cen_days; // Standard days per century, less /400 criteria
            leapd = century_num / 4; // Leap year caused by /400 criteria

            // For remaining years this century, but not current year
            jul_days += cen_years * year_days;
            if (cen_years != 0)
            {
                leapd += cen_years / 4; // Leap years this century to this year
            }

            // Calculate total days this year
            in_day += acmonthd[in_month - 1];
            if (leapf == 1 && in_month > 2)
            {
                leapd++; // A leap year & not including in in_day
            }

            // Julian days
            jul_days += in_day + leapd; // Julian date
            return (jul_days);
        }
    }
}
