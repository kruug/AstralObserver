﻿/*=============================================================================
 |       Module:  clsReadPosition.cs
 |
 |       Author:  kruug
 |     Language:  C#
 |   To Compile:  Nothing special needed.
 |
 |         Date:  2018-02-07
 |      Updated:  2018-04-05
 |
 +-----------------------------------------------------------------------------
 |
 |  Description:  Reads telescope position and updates globals.  First reads
 |                  ALT '2X1', then AZM '1X1'.
 |
 |        Input:  ccflg, 0 don't (normal), 1 correct syst coordinates.
 |
 |       Output:  None.  Updates global variables.
 |
 |    Algorithm:  
 |
 |   Known Bugs:  None.
 |
 |        Notes:  This will be shoddy until actually tested with connection
 |                  to the telescope.  I have no good way to simulate this.
 |
 *===========================================================================*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AstralObserver
{
    class clsReadPosition
    {
        string inString = "";
        double azmd = Properties.Settings.Default.azm_deg;
        double altd = Properties.Settings.Default.alt_deg;
        double ppdazm = Properties.Settings.Default.ppd_azm;
        double ppdalt = Properties.Settings.Default.ppd_alt;
        double tazmd = Properties.Settings.Default.tel_azm_deg;
        double taltd = Properties.Settings.Default.tel_alt_deg;

        /* Not Currently Used.  Saving For Later Use */
        double azmcd = Properties.Settings.Default.azm_cd;
        double altcd = Properties.Settings.Default.alt_cd;

        void readPosition(int ccflag)
        {
            int i = 0;
            long chazm, chalt;
            double chazp, chalp;
            char[] sptr;
            char[] replystr = new char[10];

            for (i = 0; i < 10; i++)
            {
                replystr[i] = ' ';
            }

            // TODO: Fix when Telescope Command Strings are implemented.
            tc_smode(10); /* '2X1' to read alt change */

            int position = inString.IndexOf('.', 0);
            if (position >= 0)
            {
              // Looking of 'X' in reply 1X1<>+nnnnnnnn
              sptr = inString.Substring(0, position - 0 + 1).Trim().ToCharArray();
            }
                
            for (i = 0; i < 9; i++)
            {
              replystr[i] = (sptr + 3 + i); // TODO: Is sptr the numbers after 'X1'?
            }

            replystr[9] = '\0';
            chalt = atol(replystr);
            
            for (i = 0; i < 10; i++)
            {
                replystr[i] = ' ';
            }

            // TODO: Fix when Telescope Command Strings are implemented.
            tc_smode(9); /* '1X1' to read azm change */

            sptr = strchr(inString, 'X');

            for (i = 0; i < 9; i++)
            {
                replystr[i] = (sptr + 3 + i); // TODO: Is sptr the numbers after 'X1'?
            }

            replystr[9] = '\0';
            chazm = atol(replystr);

            chazp = chazm;
            chalp = chalt;

            tazmd += chazp / ppdazm;
            taltd += chalp / ppdalt;

            if (tazmd < 0.0)
            {
                tazmd += 360.0; // Make positive angle
            }

            if (tazmd > 0.0)
            {
                tazmd -= 360.0; // Make angle between 0.0 and 359.9
            }

            /*
             * if (ccflag == 1)
             * {
             *   // Correct coordinate system by calculating azmcd and altcd
             *   azmcd = tazmd - azmd;
             *   altcd = taltd - altd;
             * }
             */

            azmd = tazmd;
            altd = taltd;
        }
    }
}
