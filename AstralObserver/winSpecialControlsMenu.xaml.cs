﻿/*=============================================================================
 |       Module:  winSpecialControlsMenu.cs
 |
 |       Author:  kruug
 |     Language:  C#
 |   To Compile:  Nothing special needed.
 |
 |         Date:  2017-10-26
 |      Updated:  2017-10-26
 |
 +-----------------------------------------------------------------------------
 |
 |  Description:  This is the special controls menu.  This will provide access
 |                  to all special controls of the program.  Of which, I
 |                  believe there are none.
 |
 |        Input:  No input.
 |
 |       Output:  Nothing except a GUI menu.
 |
 |    Algorithm:  GUI is built with XAML in Visual Studio
 |
 |   Known Bugs:  None so far, except only being able to run on Windows.
 |
 |        Notes:  I don't believe any of this, aside from the logger, actually
 |                  is implemented in the original program.  Probably future
 |                  functionality that will not be pursued.
 |
 *===========================================================================*/
 using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AstralObserver
{
    /// <summary>
    /// Interaction logic for winSpecialControlsMenu.xaml
    /// </summary>
    public partial class winSpecialControlsMenu : Window
    {
        public winSpecialControlsMenu()
        {
            InitializeComponent();
        }

        private void btnFilter_Click(object sender, RoutedEventArgs e)
        {
            //TODO: Not defined in original program
        }

        private void btnFocus_Click(object sender, RoutedEventArgs e)
        {
            //TODO: Not defined in original program
        }

        private void btnDeRotate_Click(object sender, RoutedEventArgs e)
        {
            //TODO: Not defined in original program
        }

        private void btnLog_Click(object sender, RoutedEventArgs e)
        {
            //TODO: Implement Logging service.
        }

        private void btnReturn_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
