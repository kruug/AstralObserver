﻿/*=============================================================================
 |       Module:  clsTestIO.cs
 |
 |       Author:  kruug
 |     Language:  C#
 |   To Compile:  Nothing special needed.
 |
 |         Date:  2017-10-25
 |      Updated:  2017-10-26
 |
 +-----------------------------------------------------------------------------
 |
 |  Description:  Connect to the telescope and verify I/O
 |
 |        Input:  None
 |
 |       Output:  Status in the form of an INT.  -1 is failure.  Anything else
 |                  is success.
 |
 |    Algorithm:  OUTLINE THE APPROACH USED BY THE MODULE TO SOLVE THE
 |                  TASK.
 |
 |   Known Bugs:  None.
 |
 |        Notes:  This will be shoddy until actually tested with connection
 |                  to the telescope.  I have no good way to simulate this.
 |
 *===========================================================================*/

using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AstralObserver
{
    class clsTestIO
    {
        int status = 0;

        public int testIO()
        {
            SerialPort guide = new SerialPort("COM1", 9600, Parity.None, 8, StopBits.Two);
            SerialPort telescope = new SerialPort("COM2", 9600, Parity.None, 8, StopBits.Two);

            System.Threading.Thread.Sleep(60000); //Delay for slow 8251 hardware
            
            char[] inString = new char[6];
            char[] portRead = new char[10];
            char[] portStatus = new char[10];
            char[] outString = new char[3];

            int attempts = 0;
            int timeout = 0;

            while (attempts < 2)
            {
                //Try to enable Compumotor RS-232
                attempts++;

                outString[0] = 'E';
                outString[1] = '\x000D'; //Carriage Return

                for (int i = 0; i < 6; i++)
                {
                    inString[i] = '0';
                }

                do
                {
                    timeout++;
                    telescope.Read(portStatus, 0, 10);
                } while ((portRead[0] & 0x00FF) != 0x000D && timeout < 512);
            }

            return status;
        }
    }
}
