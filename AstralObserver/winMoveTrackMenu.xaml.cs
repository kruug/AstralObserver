﻿/*=============================================================================
 |       Module:  winMoveTrackMenu.cs
 |
 |       Author:  kruug
 |     Language:  C#
 |   To Compile:  Nothing special needed.
 |
 |         Date:  2017-10-27
 |      Updated:  2017-10-27
 |
 +-----------------------------------------------------------------------------
 |
 |  Description:  This is the move and track menu.  This will control the
 |                  telescopes movements.
 |
 |        Input:  No input.
 |
 |       Output:  Nothing except a GUI menu.
 |
 |    Algorithm:  GUI is built with XAML in Visual Studio
 |
 |   Known Bugs:  None so far, except only being able to run on Windows.
 |
 |        Notes:  None.
 |
 *===========================================================================*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AstralObserver
{
    /// <summary>
    /// Interaction logic for winMoveTrackMenu.xaml
    /// </summary>
    public partial class winMoveTrackMenu : Window
    {
        public winMoveTrackMenu()
        {
            InitializeComponent();
        }

        private void btnRADECTrack_Click(object sender, RoutedEventArgs e)
        {
            //TODO: Implement m_mradec(0)
        }

        private void btnGuideTrack_Click(object sender, RoutedEventArgs e)
        {
            //TODO: Implement m_mradec(3)
        }

        private void btnAZALTHold_Click(object sender, RoutedEventArgs e)
        {
            //TODO: Implement m_mazalt()
        }

        private void btnHomePark_Click(object sender, RoutedEventArgs e)
        {
            //TODO: Implement m_mhompk()
        }

        private void btnJoystick_Click(object sender, RoutedEventArgs e)
        {
            //TODO: Implement m_mjoyst()
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
