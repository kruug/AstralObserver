# Code style guide

## File documentation

Every file should have a block of code formatted as below with relevant metadata and module data included.

```c
/*=============================================================================
 |       Module:  NAME OF THE MODULE
 |
 |       Author:  ACCEPTABLE TO USE PSEUDONYMS
 |     Language:  C# (CHANGE IF NOT BUT TRY TO STAY WITH THE NORM)
 |   To Compile:  EXPLAIN HOW TO COMPILE THIS MODULE (REQUIRED LIBRARIES
 |                  AND LINKS TO WHERE THESE CAN BE FOUND)
 |
 |         Date:  DATE THAT THIS MODULE WAS WRITTEN (ISO 8601: YYYY-MM-DD)
 |      Updated:  DATE OF LAST CHANGE.  ALSO NOTE IN CHANGELOG. (ISO 8601)
 |
 +-----------------------------------------------------------------------------
 |
 |  Description:  DESCRIBE THE TASK THAT THIS MODULE WAS WRITTEN TO SOLVE.
 |
 |        Input:  DESCRIBE THE INPUT THAT THE MODULE REQUIRES.
 |
 |       Output:  DESCRIBE THE OUTPUT THAT THE MODULE PRODUCES.
 |
 |    Algorithm:  OUTLINE THE APPROACH USED BY THE MODULE TO SOLVE THE
 |                  TASK.
 |
 |   Known Bugs:  IF THE PROGRAM DOES NOT FUNCTION CORRECTLY IN SOME
 |                  SITUATIONS, DESCRIBE THE SITUATIONS AND PROBLEMS HERE.
 |
 |        Notes:  ANY ADDITIONAL INFORMATION, SUCH AS WHY THE LAST CHANGE WAS
 |                  MADE AND WHERE IT WAS MADE.
 |
 *===========================================================================*/
```



## Function documentation

Every function should be preceded by the following comment block with relevant fields replaced with the necessary information.

```c
 /*------------------------------FUNCTION_NAME---------------------------------
  |  Function FUNCTION_NAME
  |
  |  Purpose:  EXPLAIN WHAT THIS FUNCTION DOES TO SUPPORT THE CORRECT
  |              OPERATION OF THE PROGRAM, AND HOW IT DOES IT.
  |
  |  Parameters:
  |      parameter_name (IN, OUT, or IN/OUT) -- EXPLANATION OF THE PURPOSE OF 
  |        THIS PARAMETER TO THE FUNCTION.
  |        (REPEAT THIS FOR ALL FORMAL PARAMETERS OF THIS FUNCTION.
  |          IN = USED TO PASS DATA INTO THIS FUNCTION,
  |          OUT = USED TO PASS DATA OUT OF THIS FUNCTION
  |          IN/OUT = USED FOR BOTH PURPOSES.)
  |
  |  Returns:  IF THIS FUNCTION SENDS BACK A VALUE VIA THE RETURN MECHANISM, 
  |              DESCRIBE THE PURPOSE OF THAT VALUE HERE.
  *--------------------------------------------------------------------------*/
```



## File layout

1. Prologue (as laid out at beginning of file) that describes what this file does and why libraries and such are included
2. Header file includes with comments if it's a non-standard include with system includes coming before user includes
3. Defines and typedefs that apply to the whole file, with constants first then function macros, with typedefs and enums last
     - If a set of defines applies to a particular piece of global data, the defines should be immediately after the data declaration and properly indented
4. Global (external) data declarations in the order:
     - Externs
     - Non-static globals
     - Static globals
5. Functions come last and should be in a meaningful order
     - Like functions should appear together
     - Functions on a similar level of abstraction should be grouped together in a breadth-first approach over depth-first
     - If functions are essentially-independent then consider alphabetical order



## Naming Conventions

1. Names with leading and trailing underscores are reserved for system purposes and should not be used for any user-created names

2. \#define constants should be in all caps

3. Enum constants are capitalized

4. Function, typedef, variables, struct, union, and enum names should all be in lower case (if multiword, follow snake case)

5. Lower-case macro names are only acceptable if the macro behaves like a function call

6. Avoid names that only differ in case (foo vs foo) or delimeter (foobar vs foo_bar)

7. Avoid ambiguous characters (l vs 1 vs | vs I)

8. Global names should have a common prefix identifying the module that they belong with



## Portability

1. Write portable code first worrying about detail optimizations only on machines where they prove necessary

2. Document performance hacks and localize them as much as possible while documenting how it works and why it was needed

3. Code that is inherently non-portable should be seperated from portable code to ease migration to new environments

4. Any behavior described as "implementation defined" should be treated as a machine dependency

5. Use lint when available to find machine-dependent constructs

6. Enable warning in your compiler



## General code styles

1. Tabs are 2 spaces

2. Spaces are preferred to tabs

3. Inclusion of braces is preferred, even on simple statements

4. Opening brace should be on the same line as the statement

5. Simple loops/switches/etc can reside on a single line, but use common sense when to break it into multiple lines.  We're going for readability, not shortest program

6. Generally, all binary operators except `.` and `->` should be separated from their operands by blanks (1 + 2 instead of 1+2).

7. Any and all dates will follow ISO 8601 (YYYY-MM-DD).  The only exception is during functions that require a Unix timestamp.  This includes any documentation that is written, as well as the changelog.

   Wrong:

   ~~~~c
   for (hour1=0; hour1<2; hour1++)
       for (hour2=0; hour2<10; hour2++)

   if (victor(human))
   {
           human_wins++;
           printf("I am your humble servant.\n");
   } else 
   {
           computer_wins++;
           printf("Your destiny is under my control!\n");
   }
   ~~~~

   Right:

   ~~~~c
   for (hour1 = 0; hour1 < 2; hour1++)
     for (hour2 = 0; hour2 < 10; hour2++)

   if (victor(human) == true) {
     human_wins++;
     printf("I am your humble servant.\n");
   } else {
     computer_wins++;
     printf("Your destiny is under my control!\n");
   }
   ~~~~



## Do's and Don'ts

1. Do explicitly comment variables that are changed out of the normal control flow or other code that is likely to break during maintenance
2. Do use the register sparingly to indicate the variables that you think are most critical
3. Do make tests explicit [(foo == true) instead of (foo)]
4. Do test floating-point numbers as <= or >=
5. Do write readable code
6. Do NOT change syntax via macro substitution
7. Do NOT use floating-point variables where discrete values are needed such as loop counters
8. Do NOT test floating-point numbers as == or !=
9. Do NOT write around compiler bugs unless you are forced to use a particular buggy compiler
10. Do NOT use beautifiers as code needs to be maintainable
