# Contributing to StarTrac

The following is a set of guidelines for contributing to StarTrac. These are mostly guidelines, not rules. Use your best judgment, and feel free to propose changes to this document in a pull request.

## Code of Conduct

This project and everyone participating in it is governed by the [Code of Conduct](guidelines\CODE_OF_CONDUCT.md). By participating, you are expected to uphold this code. Please report unacceptable behavior to [kruug@kruug.org](mailto:kruug@kruug.org). 

## How to contribute

- Found an issue?
  - Check if a bug report has already been submitted: https://kruug.org/git/kruug/StarTrac/issues
  - Use this template to properly formulate your bug report: [ISSUE_TEMPLATE.md](guidelines\ISSUE_TEMPLATE.md)
  - Submit a bug report here: https://kruug.org/git/kruug/StarTrac/issues/new
- Did you write a patch that fixes a bug?
  - Verify that your code follows the projects coding conventions: [CODE_STYLE_GUIDE.md](guidelines\CODE_STYLE_GUIDE.md)
  - Use this template to properly format your pull request: [PULL_REQUEST_TEMPLATE.md](guidelines\PULL_REQUEST_TEMPLATE.md)
  - Open a pull request: https://kruug.org/git/kruug/StarTrac/merge_requests/new

## Style guides

- [Code Style Guide](guidelines\CODE_STYLE_GUIDE.md)
- [Git Commit Messages](guidelines\GIT_COMMIT_MESSAGES.md)